const _ = require('lodash');
const { Project } = require('../../models/Project');


/**
 * Create new project
 * @param: Information Project
 * @author: Luu Phuc
 */
module.exports.create = async (req, res, next) => {
    try {
        const p = Object.assign(req.body.project, { creator: req.user._id })
        Project.create(p);
        return res.status(200).json({ message: 'Create successful' })
    } catch (e) {
        return Promise.reject(e)
    }
}
/**
 * Assign project
 * @param: id
 * @author: Luu Phuc
*/
module.exports.assign = async (req, res, next) => {
    const { id } = req.params;
    try {
        const project = await Project.findById(id);
        project.members = project.members.concat(req.body.id)
        project.save();
        return res.status(200).json({ message: 'Assign successful' })
    } catch (e) {
        return Promise.reject(e)
    }
}

/**
 * Delete project
 * @param: id
 * @author: Luu Phuc
 */

module.exports.delete = async (req, res, next) => {
    const { id } = req.params;
    Project.deleteOne({ _id: id })
        .then(() => {
            return res.status(200).json({ message: 'Delete successful' })
        })
        .catch(err => res.json(err))
}

/**
 * Find project by id
 * @param: id
 * @author: Luu Phuc
 */
module.exports.findById = async (req, res, next) => {
    const { id } = req.params;
    try {
        const project = await Project.findById(id);
        if (!project) return res.status(400).json({ message: 'Project dose not exists' })
        return res.status(200).json(project)
    } catch (e) {
        return res.json(e)
    }
}

/**
 * Find all projects
 * @author: Luu Phuc
 */
module.exports.findAll = async (req, res, next) => {
    try {
        const allProject = await Project.find({})
        if (!allProject) return Promise.reject({ message: 'Some thing wrong' })
        return res.status(200).json(allProject)
    } catch (e) {
        return res.json(e)
    }
}