const _ = require('lodash')
const config = require('../../config')
const { createToken } = require('../../middlewares/createToken');
const { cryptPassword, comparePassword } = require('../../middlewares/password');
const { User } = require('../../models/User');

/**
 * Register
 * @param: Information's user
 * @author: Luu Phuc
 */
module.exports.register = async (req, res, next) => {
    const { name, email, dateOfBirth, gender, phone } = req.body;
    try {
        const passwordHash = await cryptPassword(req.body.password)
        const newUser = new User({ ...req.body, password: passwordHash })
        await newUser.save()
        return res.status(200).json({ message: 'Register successful' })
    } catch (e) {
        return Promise.reject('Some thing wrong')
    }
}

/**
 * Login
 * @param: Email, password
 * @author: Luu Phuc
 */
module.exports.login = async (req, res, next) => {
    const { email, password } = req.body;
    try {
        const user = await User.findOne({ email });

        const isMatched = await comparePassword(req.body.password, user.password);
        if (!isMatched) return Promise.reject({ message: 'Wrong password' });

        const payload = _.pick(user, ['_id', 'role', 'email', 'name'])
        const token = await createToken(payload, '1h');

        return res.status(200).json({ message: 'Login successful', token })

    } catch (e) {
        return Promise.reject(e)
    }
}

/**
 * Update user
 * @param: id
 * @author: Luu Phuc
 */
module.exports.update = async (req, res, next) => {
    const { id } = req.params;
    try {
        const user = await User.findById(id)
        if (!user) return Promise.reject({ message: 'User dose not exists' })
        Object.keys(req.body).forEach(key => user[key] = req.body[key]);
        user.save();
        return res.status(200).json({ message: 'Update successful' })
    } catch (e) {
        return Promise.reject(e)
    }
}

/**
 * Delete user
 * @param: id
 * @author: Luu Phuc
 */
module.exports.delete = (req, res, next) => {
    const { id } = req.params;
    User.deleteOne({ _id: id })
        .then(() => {
            res.status(200).json({ message: 'Delete Successful' })
        })
        .catch(err => res.json(err))
}
