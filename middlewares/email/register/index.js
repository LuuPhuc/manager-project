const nodemailer = require('nodemailer');
const fs = require('fs'); // built-in NodeJS
const hogan = require('hogan.js');
const config = require('../../../config');

const template = fs.readFileSync(`${__dirname}/template/bookingTicket.hjs`, "utf-8");
const compiledTemplate = hogan.compile(template);

module.exports.confirmRegister = (user) => {
    const transport = {
        host: "smtp.gmail.com",
        port: 587,
        secure: false,
        requireTLS: true,
        requireSSL: true,
        auth: {
            user: config.email,
            pass: config.password
        }
    }

    const transporter = nodemailer.createTransport = (transport);

    const mailOptions = {
        from: config.email,
        to: user.email,
        subject: 'Your email register successful',
        html: compiledTemplate.render({
        })
    }

    transporter.sendMail(mailOptions, err => {
        if (err) return console.log(err.message)
        console.log('Send email success')
    })
}