const validator = require('validator');
const _ = require('lodash');
const { User } = require("../../models/User");

module.exports.validateRegisterUser = async (req, res, next) => {

    const email = _.get(req, 'body.email', '');
    const password = _.get(req, 'body.password', '');
    const confirmPassword = _.get(req, 'body.confirmPassword', '');

    try {
        if (validator.isEmpty(email)) {
            throw 'Email is required';
        } else {
            const user = await User.findOne({ email })
            if (user) {
                throw 'Email exists';
            } else if (!validator.isEmail(email)) {
                throw 'Email is invalid'
            }
        }

        if (validator.isEmpty(password)) {
            throw 'Password is required'
        } else if (!validator.isLength(password, { min: 8 })) {
            throw 'Password must have at least 8 characters'
        }

        if (validator.isEmpty(confirmPassword)) {
            throw 'Confirmed password is required'
        } else if (!validator.equals(password, confirmPassword)) {
            throw 'Password must match'
        }
        return next();
    } catch (e) {
        res.status(400).json({ message: e })
    }
}

module.exports.validateLogin = async (req, res, next) => {
    const email = _.get(req, 'body.email', '');
    const password = _.get(req, 'body.password', '');
    const confirmPassword = _.get(req, 'body.confirmPassword', '');
    try {
        if (!validator.isEmail(email)) {
            throw 'Email is invalid'
        }
        if (validator.isEmpty(password)) {
            throw 'Password is required'
        }
        if (!validator.isLength(password, { min: 8 })) {
            throw 'Password must have at least 8 characters'
        }

        const user = await User.findOne({ email })
        if (!user) {
            throw 'Email dose not exists';
        }
        return next();
    } catch (e) {
        res.status(400).json({ message: e })
    }

}