import config from '../config'
import { getHeaders } from '../utils/common'
import jwtDecode from 'jwt-decode';
import axios from 'axios';
const KEY_TOKEN = 'token';

export function getToken() {
    return sessionStorage.getItem(KEY_TOKEN);
}

export function setToken(token) {
    sessionStorage.setItem(KEY_TOKEN, token);
}

export function login(email, password) {
    return new Promise((resolve, reject) => {
        const user = {
            email,
            password
        }
        return axios.post(config.api_url + '/users/login', user)
            .then(res => {
                let token = res.data.token
                setToken(token)
                resolve(token)
            })
            .catch(err => {
                reject(err);
            });
    })
}

export function signup(email, password, name, phone, dateOfBirth) {
    return new Promise((resolve, reject) => {
        const user = {
            email,
            password,
            confirmPassword: password,
            name,
            phone,
            dateOfBirth
        }
        console.log(user)
        return axios.post(config.api_url + '/users', user)
            .then(res => {
                resolve(res.data)
            })
            .catch(err => {
                reject(err)
            })
    })
}

export function findAllUsers() {
    return new Promise((resolve, reject) => {
        // return fetch(config.api_url + '/users', {
        //     headers: getHeaders(),
        // })
        //     .then(res => res.json())
        //     .then(resJSON => {
        //         resolve(resJSON.result)
        //     })
        //     .catch(err => reject(err))
        return axios.get(config.api_url + '/users', {
            headers: getHeaders(),
        })
            .then(res => {
                resolve(res.data.result)
            })
    })
}

export function findByID(userID) {
    return new Promise(async (resolve, reject) => {
        return fetch(config.api_url + '/users/' + userID, {
            headers: getHeaders(),
        })
            .then(res => res.json())
            .then(resJSON => {
                resolve(resJSON.result)
            })
            .catch(err => reject(err))
    })
}

export function isLoggedIn() {
    const tok = getToken();
    return !!tok;
}

export function signout() {
    sessionStorage.removeItem(KEY_TOKEN);
}

export function requireAuth(nextState, replace) {
    if (!isLoggedIn()) {
        replace({ pathname: '/' });
    }
}

export function getTokenData() {
    let token = sessionStorage.getItem(KEY_TOKEN);
    return jwtDecode(token)
}