const express = require('express');
const UserController = require('../../controller/user')
const { validateRegisterUser, validateLogin } = require("../../middlewares/validatation/user");


const router = express.Router();

router.post('/', validateRegisterUser, UserController.register);
router.post('/login', validateLogin, UserController.login);

module.exports = router;