const express = require('express');
const ProjectController = require('../../controller/project')
const { authenticate, authorize } = require('../../middlewares/auth');

const router = express.Router();

router.post('/create', authenticate, authorize(['admin']), ProjectController.create);
router.post('/assign', authenticate, authorize(['admin']), ProjectController.assign);

router.get('/:id', authenticate, authorize(['admin']), ProjectController.findById);
router.get('/', authenticate, authorize(['admin']), ProjectController.findAll);

module.exports = router;